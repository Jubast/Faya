# FAYA

Faya is an android application that reminds me to put wood into the fireplace


## Icon <img src="https://gitlab.com/Jubast/Faya/raw/master/app/src/main/res/mipmap-hdpi/ic_faya.png" height="50px" width="50px"/>
| Info | Comment |
| ------ | ------ |
| Source | https://www.iconfinder.com/icons/384893/chimney_christmas_fire_fireplace_icon |
| Changes | Background color |

## Screenshots
- TODO