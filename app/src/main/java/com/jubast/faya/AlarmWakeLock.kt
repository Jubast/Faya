package com.jubast.faya

import android.annotation.SuppressLint
import android.content.Context
import android.os.PowerManager
import java.util.concurrent.TimeUnit

// Some phones stop executing code (go idle) before the activity starts
// and can take over with the wake lock
object AlarmWakeLock {
    @SuppressLint("InvalidWakeLockTag")
    fun acquire(context: Context) {
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, AlarmSettings.WAKE_LOCK_TAG)

        // used to keep the device running
        // so the phone doesn't go idle before the activity starts
        // and WindowManager takes over the screen
        wakeLock.acquire(TimeUnit.SECONDS.toMillis(5))
    }
}