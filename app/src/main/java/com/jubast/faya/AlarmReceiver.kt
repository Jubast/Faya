package com.jubast.faya

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.jubast.faya.activitys.AlarmActivity

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d("LOG.DD", "Alarm Received!")
        when(intent.action)
        {
            AlarmSettings.TIME_ELAPSED -> {
                startAlarm(context)
            }
            else -> return
        }
    }

    private fun startAlarm(context: Context)
    {
        AlarmWakeLock.acquire(context)

        // writes down that the alarm has been called
        AlarmPreferences(context).isAlarmSet = false

        val intent = Intent(context, AlarmActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}