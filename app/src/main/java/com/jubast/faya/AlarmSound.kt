package com.jubast.faya

import android.content.Context
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.os.Handler
import android.util.Log

class AlarmSound(val context: Context) : MediaPlayer() {
    private val musicFileUri = ResourceHelper.getResourceUri(context, R.raw.fire_audio)
    private val handler = Handler()
    private val delay: Long = 150
    private var volume: Float = 0.3f

    init {
        setAudioAttributes(
            AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_ALARM)
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .build()
        )
        setDataSource(context, musicFileUri)
        isLooping = true
        prepare()

        // volume raising over time
        setVolume(volume, volume)
        handler.postDelayed(this::changeVolume, delay)
    }

    private fun changeVolume(){
        volume += 0.025f

        if(volume < 1f){
            handler.postDelayed(this::changeVolume, delay)
        }

        if(volume >= 1f){
            volume = 1f
        }

        Log.d("LOG.DD", volume.toString())
        setVolume(volume, volume)
    }
}