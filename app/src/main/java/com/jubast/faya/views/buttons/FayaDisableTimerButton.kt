package com.jubast.faya.views.buttons

import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.Toast
import com.jubast.faya.AlarmHelper

@SuppressLint("ViewConstructor")
class FayaDisableTimerButton private constructor(context: Context, text: String) : BaseButton<FayaDisableTimerButton>(context) {
    companion object {
        fun create(context: Context, text: String): FayaDisableTimerButton {
            return FayaDisableTimerButton(context, text).also{ button ->
                button.onCreate()
            }
        }
    }
    override val eventObject: FayaDisableTimerButton = this
    init {
        this.text = text
        onClick(this::disableAlarms)
    }

    private fun disableAlarms(button: FayaDisableTimerButton){
        AlarmHelper.clearAlarms(context)
        Toast.makeText(context, "Alarm disabled!", Toast.LENGTH_LONG).show()
    }

    override fun setLayoutPosition() {
        super.setLayoutPosition()
        (layoutParams as LinearLayout.LayoutParams).also {
            it.bottomMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, resources.displayMetrics).toInt()
            it.marginStart = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics).toInt()
            it.marginEnd = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics).toInt()
            it.width = LinearLayout.LayoutParams.MATCH_PARENT
            // TODO: Check if height can be WRAP_CONTENT
            it.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, resources.displayMetrics).toInt()
            it.weight = 1f
        }
    }
}