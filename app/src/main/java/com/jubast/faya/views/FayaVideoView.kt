package com.jubast.faya.views

import android.content.Context
import android.graphics.Matrix
import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.media.PlaybackParams
import android.util.AttributeSet
import android.util.Log
import android.view.Surface
import android.view.TextureView
import com.jubast.faya.ResourceHelper
import com.jubast.faya.views.exceptions.NoVideoSourceException
import java.lang.IllegalStateException

// Only tested with an 1920x1080 video
// Don't know if center corp will work with other resolution
class FayaVideoView : TextureView, TextureView.SurfaceTextureListener{
    private val videoPlayer = MediaPlayer()
    private var isDataSourceSet: Boolean = false

    constructor(context: Context) : super(context)
    constructor (context: Context, attrs: AttributeSet): super(context, attrs)
    constructor (context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    init {
        surfaceTextureListener = this
        videoPlayer.isLooping = true
    }

    fun setDataSource(resource: Int){
        videoPlayer.setDataSource(context, ResourceHelper.getResourceUri(context, resource))
        videoPlayer.prepare()
        videoPlayer.playbackParams = videoPlayer.playbackParams.setSpeed(0.8f)
        isDataSourceSet = true
    }

    fun play(){
        if(!isDataSourceSet){
            throw NoVideoSourceException("The video source was not set (setDataSource)")
        }
        videoPlayer.start()
    }

    fun pause(){
        videoPlayer.pause()
    }

    fun stop(){
        videoPlayer.stop()
    }

    private fun centerCorp(){
        val videoHeight = videoPlayer.videoHeight.toFloat()
        val videoWidth = videoPlayer.videoWidth.toFloat()

        val viewHeight = height.toFloat()
        val viewWidth = width.toFloat()

        val scaleY: Float = 1f

        // By settings the scaleY to 1, the video Y will be internally scaled.
        // to keep the aspect ratio we also have to scale X by the same amount
        // this happens in the below true block
        val internalScaleY = viewHeight / videoHeight
        val scaleX: Float = videoWidth / viewWidth * internalScaleY

        Log.d("LOG.DD", "scaleX: $scaleX")
        Log.d("LOG.DD", "scaleY: $scaleY")

        val matrix = Matrix()
        matrix.setScale(scaleX, scaleY, viewWidth / 2f, viewHeight / 2f)

        setTransform(matrix)
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {}

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {}

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean { return false }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        videoPlayer.setSurface(Surface(surfaceTexture))
        centerCorp()
    }
}