package com.jubast.faya.views.buttons

import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.Toast
import com.jubast.faya.AlarmHelper

@SuppressLint("ViewConstructor")
class FayaSetTimerButton private constructor(context: Context, text: String, val time: Short): BaseButton<FayaSetTimerButton>(context),
    IButtonStyle {
    companion object {
        fun create(context: Context, text: String, time: Short): FayaSetTimerButton {
            return FayaSetTimerButton(context, text, time).also{ button ->
                button.onCreate()
            }
        }
    }
    override val eventObject: FayaSetTimerButton = this
    init {
        this.text = text

        // when the button is clicked, set the timer
        onClick(this::createTimer)
    }

    /**
     * Creates the alarm
     * The length of the alarm comes from the tag
     */
    private fun createTimer(button: FayaSetTimerButton){
        when(AlarmHelper.setAlarm(context, button.time.toLong())){
            true -> Toast.makeText(context, "Alarm started!", Toast.LENGTH_LONG).show()
            else ->  Toast.makeText(context, "Alarm already exists!", Toast.LENGTH_LONG).show()
        }
    }

    override fun setLayoutPosition(){
        super.setLayoutPosition()
        (layoutParams as LinearLayout.LayoutParams).also{
            it.topMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, resources.displayMetrics).toInt()
            it.marginStart = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics).toInt()
            it.marginEnd = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics).toInt()
            it.height = LinearLayout.LayoutParams.WRAP_CONTENT
            it.width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, resources.displayMetrics).toInt()
            it.weight = 1f
        }
    }
}
