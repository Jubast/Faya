package com.jubast.faya.views.exceptions

import java.lang.Exception

class NoVideoSourceException(msg: String): Exception(msg)