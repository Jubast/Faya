package com.jubast.faya.views.buttons

// WHY IS NAMING THINGS SO HARD???
interface IButtonStyle {
    /**
     * Sets the margin, height, width settings for the button
     */
    fun setLayoutPosition()
    /**
     * Sets looks of the button
     * Sets the gravity, text size, text style and background color
     */
    fun setButtonLooks()
}