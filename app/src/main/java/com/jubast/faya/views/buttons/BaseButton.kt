package com.jubast.faya.views.buttons

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.TypedValue
import android.view.Gravity
import android.widget.Button
import android.widget.LinearLayout
import com.jubast.faya.events.Event

abstract class BaseButton<T>(context: Context) : Button(context), IButtonStyle {
    private val clickEvent: Event<T> = Event<T>()
    protected abstract val eventObject: T

    init {
        setOnClickListener { clickEvent(eventObject) }
    }

    open fun onCreate(){
        setLayoutPosition()
        setButtonLooks()
    }

    override fun setLayoutPosition() {
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    }

    override fun setButtonLooks() {
        // this is the text gravity
        gravity = Gravity.CENTER

        setTextSize(TypedValue.COMPLEX_UNIT_SP, 40f)
        setTypeface(typeface, Typeface.BOLD)

        setBackgroundColor(Color.RED)
        background.alpha = 128
    }

    fun onClick(func: (T) -> Unit){
        clickEvent += func
    }
}