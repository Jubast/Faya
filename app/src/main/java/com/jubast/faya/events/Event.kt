package com.jubast.faya.events

open class Event<T> {
    private val handlers = mutableListOf<(T) -> Unit>()

    operator fun minusAssign(func: (T) -> Unit) {
        handlers.remove(func)
    }
    operator fun plusAssign(func: (T) -> Unit){
        handlers.add(func)
    }

    operator fun invoke(args: T){
        for(handler in handlers){
            handler(args)
        }
    }
}