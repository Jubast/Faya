package com.jubast.faya.activitys

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jubast.faya.AlarmSettings
import com.jubast.faya.R
import com.jubast.faya.views.buttons.FayaDisableTimerButton
import com.jubast.faya.views.buttons.FayaSetTimerButton
import kotlinx.android.synthetic.main.activity_faya_base.*

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faya_base)

        // activity background video
        setBackgroundVideo(R.raw.fire_video)

        // creates buttons and adds them to the activity layout
        setButtons()
    }

    /**
     * Sets the activity background video
     */
    private fun setBackgroundVideo(resource: Int){
        vvPlayer.setDataSource(resource)
        vvPlayer.play()
    }

    /**
     * Creates the buttons and adds them to the activity layout
     */
    private fun setButtons(){
        Array<FayaSetTimerButton>(AlarmSettings.TIMERS.size) { i ->
            FayaSetTimerButton.create(this, "${AlarmSettings.TIMERS[i]} MIN", AlarmSettings.TIMERS[i].toShort())
                .also { button ->
                    llTop.addView(button)
                    onTimerButtonAdded(button)
            }
        }

        FayaDisableTimerButton.create(this, "Clear timers").also{ button ->
            llBottom.addView(button)
            onDisableTimerButtonAdded(button)
        }
    }

    /**
     * Gets called when a button is added to the activity layout
     */
    protected open fun onTimerButtonAdded(button: FayaSetTimerButton){
        // Nothing to do
    }

    /**
     * Gets called when a button is added to the activity layout
     */
    protected open fun onDisableTimerButtonAdded(button: FayaDisableTimerButton){
        // Nothing to do
    }
}