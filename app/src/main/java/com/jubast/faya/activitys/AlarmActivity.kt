package com.jubast.faya.activitys

import android.app.KeyguardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import com.jubast.faya.AlarmPreferences
import com.jubast.faya.AlarmSound
import com.jubast.faya.views.buttons.BaseButton
import com.jubast.faya.views.buttons.FayaDisableTimerButton
import com.jubast.faya.views.buttons.FayaSetTimerButton

class AlarmActivity : BaseActivity() {
    private lateinit var alarmSound: AlarmSound

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showOnLockedScreen()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun showOnLockedScreen(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)

            val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            keyguardManager.requestDismissKeyguard(this, null)
        }
        else {
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        }
    }

    // close the activity when button get clicked
    override fun onTimerButtonAdded(button: FayaSetTimerButton) {
        super.onTimerButtonAdded(button)
        button.onClick(this::onTimerButtonClick)
    }

    // close the activity when button get clicked
    override fun onDisableTimerButtonAdded(button: FayaDisableTimerButton) {
        super.onDisableTimerButtonAdded(button)
        button.onClick(this::onTimerButtonClick)
    }

    private fun onTimerButtonClick(button: BaseButton<*>){
        exitApplication()
    }

    override fun onBackPressed() {
        exitApplication()
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()

        if(!this::alarmSound.isInitialized){
            alarmSound = AlarmSound(this)
        }

        alarmSound.start()
    }

    override fun onPause() {
        alarmSound.pause()
        super.onPause()
    }

    private fun exitApplication(){
        alarmSound.stop()
        finishAndRemoveTask()
    }
}