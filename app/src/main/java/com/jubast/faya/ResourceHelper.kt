package com.jubast.faya

import android.content.Context
import android.net.Uri

object ResourceHelper {

    fun getResourceUri(context: Context, resource: Int): Uri{
        return Uri.parse("android.resource://${context.packageName}/$resource")
    }
}