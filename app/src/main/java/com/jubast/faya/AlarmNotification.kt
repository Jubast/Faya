package com.jubast.faya

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat

object AlarmNotification {
    private const val NOTIFICATION_ID = 0
    private const val NOTIFICATION_CHANNEL_ID = "com.jubast.faya.notification.default_channel_id"
    private const val NOTIFICATION_CHANNEL_NAME = "DEFAULT"
    private const val NOTIFICATION_CHANNEL_DESCRIPTION = "DEFAULT NOTIFICATIONS AKA. THIS APP ONLY USES 1 NOTIFICATION"

    fun create(context: Context){
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(context, manager)
        }

        val notification = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID).also {
            it.setSmallIcon(R.drawable.ic_fire)
            it.setContentTitle("FAYA TIMER")
            it.setContentText("A FAYA TIMER IS ACTIVE!")
            it.setOngoing(true)
            it.priority = NotificationCompat.PRIORITY_DEFAULT
        }.build()

        manager.notify(NOTIFICATION_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel(context: Context, manager: NotificationManager){
        NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_DEFAULT).also{
            it.description = NOTIFICATION_CHANNEL_DESCRIPTION
            manager.createNotificationChannel(it)
        }
    }

    fun remove(context: Context){
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(NOTIFICATION_ID)
    }
}