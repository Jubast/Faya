package com.jubast.faya

import android.content.Context
import android.content.SharedPreferences

class AlarmPreferences(val context: Context) {
    private val _name = "com.jubast.faya.alarm"

    var isAlarmSet: Boolean
        get() = getSharedPreferences().getBoolean(this::_name.name, false)
        set(value) = getSharedPreferencesEditor().putBoolean(this::_name.name, value).apply()

    private fun getSharedPreferencesEditor(): SharedPreferences.Editor{
        return getSharedPreferences().edit()
    }

    private fun getSharedPreferences(): SharedPreferences{
        return context.getSharedPreferences(_name, Context.MODE_PRIVATE)
    }
}