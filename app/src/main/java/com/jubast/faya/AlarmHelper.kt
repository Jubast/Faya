package com.jubast.faya

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.concurrent.TimeUnit

object AlarmHelper {
    /**
     * Sets alarm in {minutes} that will call Faya receiver
     * In DEBUG build its always 5sec
     */
    fun setAlarm(context: Context, time: Long): Boolean {
        // checks if alarm exists
        val alarmPreferences = AlarmPreferences(context)
        if(alarmPreferences.isAlarmSet){
            return false
        }

        // sets the alarm
        context.getSystemService(AlarmManager::class.java)
            .setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP, System.currentTimeMillis() +
                    getMillis(time), getAlarmPendingIntent(context))

        // writes down that the alarm is set
        alarmPreferences.isAlarmSet = true

        AlarmNotification.create(context)
        return true
    }

    private fun getMillis(time: Long): Long {
        return when(BuildConfig.DEBUG){
            true -> TimeUnit.SECONDS.toMillis(5)
            else -> TimeUnit.MINUTES.toMillis(time)
        }
    }

    fun clearAlarms(context: Context) {
        // cancels the alarm
        context.getSystemService(AlarmManager::class.java).cancel(getAlarmPendingIntent(context))

        // writes down that the alarm is canceled
        AlarmPreferences(context).isAlarmSet = false

        AlarmNotification.remove(context)
    }

    private fun getAlarmPendingIntent(context: Context): PendingIntent {
        val intent = Intent(context, AlarmReceiver::class.java)
        intent.action = AlarmSettings.TIME_ELAPSED
        return PendingIntent.getBroadcast(context, 0, intent, 0)
    }
}