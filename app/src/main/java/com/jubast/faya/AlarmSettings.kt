package com.jubast.faya

object AlarmSettings {
    const val WAKE_LOCK_TAG: String = "com.jubast.FAYA_WAKE_LOCK_TAG"
    const val TIME_ELAPSED: String = "intent.jubast.FAYA_TIME_ELAPSED"
    val TIMERS: Array<String> = arrayOf("15", "30", "45", "60")
}